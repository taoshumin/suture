/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"github.com/shumintao/logger"
	"github.com/suture/events"
	"github.com/suture/svcutil"
	"github.com/thejerf/suture/v4"
	"time"
)

type PanicServer struct{}

func (s *PanicServer) Serve(ctx context.Context) error {
	fmt.Println("Hello")
	for {
		time.Sleep(3 * time.Second)
		panic("painc")
	}
	return nil
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// PassThroughPanics: true 则恐慌后会奔溃
	spec := svcutil.SpecWithInfoLogger(logger.New(logger.DebugLevel))
	earlyService := suture.New("early", spec)
	earlyService.ServeBackground(ctx)

	evLogger := events.NewLogger()
	earlyService.Add(evLogger)

	paic := new(PanicServer)
	earlyService.Add(paic)

	for {

	}
}
