/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package events

import (
	"context"
	"sync"
	"testing"
	"time"
)

const timeout = time.Second

func init() {
	runningTests = true
}

func TestNewLogger(t *testing.T) {
	l := NewLogger()
	if l == nil {
		t.Fatal("Unexpected nil Logger")
	}
}

func setupLogger() (Logger, context.CancelFunc) {
	ctx, cancel := context.WithCancel(context.Background())
	l := NewLogger()
	go l.Serve(ctx)
	return l, cancel
}

func TestSubscribe(t *testing.T) {
	l, cancel := setupLogger()
	defer cancel()

	s := l.Subscribe(AllEvents)
	defer s.Unsubscribe()

	l.Log(DeviceConnected, "foo")

	ev, err := s.Poll(timeout)
	if err != nil {
		t.Fatal("Unexpected error:", err)
	}
	if ev.Type != DeviceConnected {
		t.Error("Incorrect event type", ev.Type)
	}

	switch v := ev.Data.(type) {
	case string:
		if v != "foo" {
			t.Error("Incorrect Data string", v)
		}
	default:
		t.Errorf("Incorrect Data type %#v", v)
	}
}

func TestGlobalIDs(t *testing.T) {
	l, cancel := setupLogger()
	defer cancel()

	s := l.Subscribe(AllEvents)
	defer s.Unsubscribe()

	l.Log(DeviceConnected, "foo")
	l.Subscribe(AllEvents)
	l.Log(DeviceConnected, "bar")

	ev, err := s.Poll(timeout)
	if err != nil {
		t.Fatal("Unexpected error:", err)
	}
	if ev.Data.(string) != "foo" {
		t.Fatal("Incorrect event:", ev)
	}
	id := ev.GlobalID

	ev, err = s.Poll(timeout)
	if err != nil {
		t.Fatal("Unexpected error:", err)
	}
	if ev.Data.(string) != "bar" {
		t.Fatal("Incorrect event:", ev)
	}
	if ev.GlobalID != id+1 {
		t.Fatalf("ID not incremented (%d != %d)", ev.GlobalID, id+1)
	}
}

func TestSubscriptionIDs(t *testing.T) {
	l, cancel := setupLogger()
	defer cancel()

	s := l.Subscribe(DeviceConnected)
	defer s.Unsubscribe()

	l.Log(DeviceDisconnected, "a")
	l.Log(DeviceConnected, "b")
	l.Log(DeviceConnected, "c")
	l.Log(DeviceDisconnected, "d")

	ev, err := s.Poll(timeout)
	if err != nil {
		t.Fatal("Unexpected error:", err)
	}

	if ev.GlobalID != 2 {
		t.Fatal("Incorrect GlobalID:", ev.GlobalID)
	}
	if ev.SubscriptionID != 1 {
		t.Fatal("Incorrect SubscriptionID:", ev.SubscriptionID)
	}

	ev, err = s.Poll(timeout)
	if err != nil {
		t.Fatal("Unexpected error:", err)
	}
	if ev.GlobalID != 3 {
		t.Fatal("Incorrect GlobalID:", ev.GlobalID)
	}
	if ev.SubscriptionID != 2 {
		t.Fatal("Incorrect SubscriptionID:", ev.SubscriptionID)
	}

	ev, err = s.Poll(timeout)
	if err != ErrTimeout {
		t.Fatal("Unexpected error:", err)
	}
}

func TestUnsubscribeContention(t *testing.T) {
	// Check that we can unsubscribe without blocking the whole system.

	const (
		listeners = 50
		senders   = 1000
	)

	l, cancel := setupLogger()
	defer cancel()

	// Start listeners. These will poll until the stop channel is closed,
	// then exit and unsubscribe.

	stopListeners := make(chan struct{})
	var listenerWg sync.WaitGroup
	listenerWg.Add(listeners)
	for i := 0; i < listeners; i++ {
		go func() {
			defer listenerWg.Done()

			s := l.Subscribe(AllEvents)
			defer s.Unsubscribe()

			for {
				select {
				case <-s.C():

				case <-stopListeners:
					return
				}
			}
		}()
	}

	// Start senders. These send pointless events until the stop channel is
	// closed.

	stopSenders := make(chan struct{})
	defer close(stopSenders)
	var senderWg sync.WaitGroup

	senderWg.Add(senders)
	for i := 0; i < senders; i++ {
		go func() {
			defer senderWg.Done()

			t := time.NewTicker(time.Millisecond)

			for {
				select {
				case <-t.C:
					l.Log(Starting, nil)

				case <-stopSenders:
					return
				}
			}
		}()
	}

	// Give everything time to start up.

	time.Sleep(time.Second)

	// Stop the listeners and wait for them to exit. This should happen in a
	// reasonable time frame.

	t0 := time.Now()
	close(stopListeners)
	listenerWg.Wait()
	if d := time.Since(t0); d > time.Minute {
		t.Error("It should not take", d, "to unsubscribe from an event stream")
	}
}
