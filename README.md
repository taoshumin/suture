# suture


简介： Supervision Tree 监督树，监控树就是一种以树的形式管理多个进程/线程/协程（后面统一说进程）的程序设计模型。


- events: 日志事件订阅
- svcutil: 监督树
- sync: 锁
- test: 普通suture使用方式

bug: 会有死锁的问题`spec := svcutil.SpecWithDebugLogger(l)`


# syncthing 重启设计

`有一个监控服务monitor监控系统退出。监听：AsFatalErr`