/*
Copyright 2022 The Workpieces LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package svcutil_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/suture/svcutil"
)

func TestExitStatus(t *testing.T) {
	assert.Equal(t, svcutil.ExitSuccess.AsInt(), 0)
	assert.Equal(t, svcutil.ExitError.AsInt(), 1)
	assert.Equal(t, svcutil.ExitNoUpgradeAvailable.AsInt(), 2)
	assert.Equal(t, svcutil.ExitRestart.AsInt(), 3)
	assert.Equal(t, svcutil.ExitUpgrade.AsInt(), 4)
}
