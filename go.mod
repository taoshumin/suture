module github.com/suture

go 1.18

require (
	github.com/gogo/protobuf v1.3.2
	github.com/sasha-s/go-deadlock v0.3.1
	github.com/shumintao/logger v1.0.5
	github.com/stretchr/testify v1.7.0
	github.com/thejerf/suture/v4 v4.0.2
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/petermattis/goid v0.0.0-20220824145935-af5520614cb6 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/sirupsen/logrus v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220928140112-f11e5e49a4ec // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
